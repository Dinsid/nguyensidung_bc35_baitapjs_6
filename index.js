// Bài 1 : Tìm số nguyên dương nhỏ nhất

document.querySelector('.btn-primary').addEventListener('click', function() {
    var result_1 = document.querySelector('.bg-primary');
    var s = 0;
    var n = 1;
    while (s < 10000) {
        n++;
        s = s + n;
    }
    result_1.innerHTML = `Kết quả : Số nguyên dương nhỏ nhất là : ${n}`;
});



// Bài 2 : Tính tổng

document.querySelector('.btn-success').addEventListener('click', function() {
    var result_2 = document.querySelector('.bg-success');
    var numberX = +document.querySelector('.number-x').value;
    var numberN = +document.querySelector('.number-n').value;
    var numberS = 0;
    var numberT = 1;
    for(var a = 1; a <= numberN; a++) {
        numberT = numberT * numberX;
        numberS = numberS + numberT;
    }
    result_2.innerHTML = `Kết quả : ${numberS}`;
});



// Bài 3 : Tính giai thừa

document.querySelector('.btn-secondary').addEventListener('click', function() {
    var result_3 = document.querySelector('.bg-secondary');
    var integer = +document.querySelector('.integer').value;
    var total = 1;
    for(var i = 1; i <= integer; i++) {
        total *= i;
    }
    result_3.innerHTML = `Kết quả : Giai thừa : ${total}`;
});



// Bài 4 : Tạo thẻ div

document.querySelector('.btn-dark').addEventListener('click', function() {
    var result_4 = document.querySelector('.bg-dark');
    var contentDiv = "";
    for(var j = 1; j <= 10; j++) {
        if(j % 2 == 0) {
            var div = `<div class='bg-danger py-2 pl-2'>Div chắn</div>`;
            contentDiv += div;
        } else {
            div = `<div class='bg-primary py-2 pl-2'>Div lẻ</div>`;
            contentDiv += div;
        }
    }
    result_4.innerHTML = `Kết quả : ${contentDiv}`;
});



